#!/bin/bash

#function get size file
getSizeFile(){
	
	if [ -z "$1" ]
	then
		echo "null"
	else
		fileSize=$(stat -c %s $1)
		echo $fileSize
	fi		
}

#For cycles and if in cycles
echo "The directory: $PWD contains the files:"
fileSize=0
maxSizeFile=0
for file in *
do
fileSize="$(getSizeFile $file)"
if [ "$fileSize" -gt "$maxSizeFile" ]; then
    maxSizeFile=$fileSize
fi 
echo -e "\t$file\t$fileSize Kb"
done
echo "maxSizeFile = $maxSizeFile Kb"

#While For cycles  and If in cycles
echo -e "\nFiles in ascending size"
index=0
for file in *
do
	array[$index]=$file
	index=$(($index+1))
done
for ((a=1;a < ${#array[*]};a++))
do
	x=${array[$a]}
	b=$a

	while [ $b -gt 0 ] && [ "$(getSizeFile ${array[$b-1]})" -gt "$(getSizeFile $x)" ]
		do
			array[$b]=${array[$b-1]}
			b=$(($b-1))
		done
	array[$b]=$x
done

for ((a=0;a < ${#array[*]};a++))
do
echo -e "\t$a:\t${array[$a]}\t$(getSizeFile ${array[$a]}) Kb"
done
	
